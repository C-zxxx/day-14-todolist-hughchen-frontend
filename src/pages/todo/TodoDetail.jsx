import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Card, Tag } from "antd";
import * as api from "../../apis/todo";

import "./TodoDetail.css";

const TodoDetail = () => {
  const navigate = useNavigate();
  const params = useParams();

  const id = params.id;

  const [todoItem, setTodoItem] = useState({});

  useEffect(() => {
    api
      .getTodoById(id)
      .then((res) => {
        setTodoItem(res.data);
        if (!res.data) {
          navigate("/404");
        }
      })
      .catch((err) => {});
  }, [id, navigate]);

  return (
    <div className="todoDetail">
      <div className="title"> Detail </div>

      <Card
        className="detailCard"
        hoverable
        title="Todo Item"
        bordered={false}
        style={{
          width: "90%",
        }}
      >
        <span className="itemTitle">Content:</span> <div className="text">{todoItem && todoItem.text}</div>
        <br></br>
        <span className="itemTitle">Status:</span>
        {todoItem && todoItem.done ? <Tag color="green">DONE</Tag> : <Tag color="orange">To be completed</Tag>}
      </Card>
    </div>
  );
};

export default TodoDetail;
