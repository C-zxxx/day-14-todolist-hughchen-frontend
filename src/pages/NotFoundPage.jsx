import React from "react";
import { Button, Result } from "antd";
import { useNavigate } from "react-router-dom";
import "./NotFoundPage.css"
const NotFoundPage = () => {
  const navigator = useNavigate();
  return (
    <div className="notFoundPage">
      <Result
        status="404"
        title="404"
        subTitle="The page seems to be missing..."
        extra={
          <Button type="primary" onClick={() => navigator("/")}>
            Back Home
          </Button>
        }
      />
    </div>
  );
};

export default NotFoundPage;
