import request from "./request";

export const loadTodos = () => {
  return request.get("");
};

export const updateTodo = (id, todo) => {
  return request.put(`${id}`, todo);
};

export const addTodo = (item) => {
  return request.post("", item);
};

export const deleteTodo = (id) => {
  return request.delete(`${id}`);
};

export const getTodoById = (id) => {
  return request.get(`${id}`);
};
