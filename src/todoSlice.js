
import { createSlice, current } from '@reduxjs/toolkit'

export const todoSlice = createSlice({
  name: "todo",
  initialState: {
    todoList: [],
  },
  reducers: {

    initTodos: (state, action) => {
      // state.todoList = action.payload
      return { todoList: action.payload }
    },

    onChangeStatus: (state, action) => {
      const id = action.payload
      const newList = state.todoList.slice()
      newList.forEach(item => {
        if (item.id === id) {
          item.done = !item.done
        }
      })
      state.todoList = newList
    },

    onDeleteIcon: (state, action) => {
      // console.log(current(state));
      const id = action.payload
      const newList = state.todoList.slice()

      newList.forEach((item, index) => {
        if (item.id === id) {
          newList.splice(index, 1)
        }
      })
      state.todoList = newList
    },

    onSaveEdit: (state, action) => {
      const id = action.payload.id
      const newText = action.payload.newText
      const newList = state.todoList.slice()
      newList.forEach(item => {
        if (item.id === id) {
          item.text = newText
        }
      })
      state.todoList = newList
    }
  },
})


export const { onChangeStatus, onDeleteIcon, onSaveEdit, initTodos } = todoSlice.actions

export default todoSlice.reducer
